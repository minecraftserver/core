package fr.frivec.core.json;

import java.text.DateFormat;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;

import fr.frivec.core.console.Logger;
import fr.frivec.core.console.Logger.LogLevel;

public class GsonManager {
	
	private static GsonManager instance;
	
	private Gson gson;
	
	public GsonManager(final List<TypeAdapter<?>> adapters, final List<Class<?>> clazz) {
		
		instance = this;
		
		this.gson = createGsonInstance(adapters, clazz);
		
	}
	
	private Gson createGsonInstance(final List<TypeAdapter<?>> adapters, final List<Class<?>> clazz) {
		
		final GsonBuilder builder = new GsonBuilder()
									.setDateFormat(DateFormat.SHORT)
									.setPrettyPrinting()
									.serializeNulls()
									.disableHtmlEscaping();
		
		if(adapters == null || clazz == null) {
			
			Logger.log(LogLevel.INFO, "No Adapters or class have been found in GsonManager. No Adapter will be use.", null);
			
			return builder.create();
			
		}
		
		if(adapters.size() != clazz.size()) {
			
			Logger.log(LogLevel.WARNING, "Adapters from GsonManager doesn't have the same size. Unable to continue.", getClass());
			
			return builder.create();
			
		}
		
		for(int i = 0; i < adapters.size(); i++)
			
			builder.registerTypeAdapter(clazz.get(i), adapters.get(i));
		
		return builder.create();
	}
	
	public String serializeObject(final Object object) {
		return this.gson.toJson(object);
	}
	
	public Object deSeralizeJson(final String json, Class<?> object) {
		return this.gson.fromJson(json, object);
	}
	
	public Gson getGson() {
		return gson;
	}
	
	public static GsonManager getInstance() {
		return instance;
	}

}
