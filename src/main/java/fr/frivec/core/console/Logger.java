package fr.frivec.core.console;

public class Logger {
	
	public static void log(final LogLevel level, final String message, final Class<?> classObject) {
		
		System.out.println(level.getColorCode() + "[" + level + "] " + message + ConsoleColors.PURPLE + ((!level.equals(LogLevel.INFO)) ? " - " + classObject.getName() : "") + ConsoleColors.RESET);
		
	}
	
	public static void log(final String message) {
		
		System.out.println(message + ConsoleColors.RESET);
		
	}
	
	public enum LogLevel {
		
		INFO(ConsoleColors.GREEN),
		WARNING(ConsoleColors.YELLOW),
		SEVERE(ConsoleColors.RED),
		DEBUG(ConsoleColors.PURPLE);
		
		private String colorCode;
		
		private LogLevel(final String colorCode) {
			
			this.colorCode = colorCode;
			
		}
		
		public String getColorCode() {
			return colorCode;
		} 
		
	}

}
