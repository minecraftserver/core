package fr.frivec.core.console.commands;

public abstract class Command {
	
	protected String name,
					description,
					usage;
	
	public Command(final String name, final String description, final String usage) {
		
		this.name = name;
		this.description = description;
		this.usage = usage;
		
	}
	
	public abstract void onCommand(final String[] args);

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

}
