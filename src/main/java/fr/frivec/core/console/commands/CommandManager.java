package fr.frivec.core.console.commands;

import static fr.frivec.core.console.Logger.log;

import java.util.HashMap;
import java.util.Map;

import fr.frivec.core.console.Logger.LogLevel;

public class CommandManager {
	
	private Map<String, Command> commands;
	
	public CommandManager() {
		
		this.commands = new HashMap<>();
		
	}
	
	//Add a new command to the list. Will be listened later.
	public void registerCommand(final String key, final Command command) {
		
		if(!this.commands.containsKey(key) || !this.commands.containsValue(command)) {
			
			this.commands.put(key, command);
			log(LogLevel.INFO, "Registered " + key + " command.", this.getClass());
			
		}
		
	}
	
	//Remove the command from the list
	public void removeCommand(final String key) {
		
		if(this.commands.containsKey(key)) {
			
			this.commands.remove(key);
			log(LogLevel.INFO, "Removed " + key + " command.", this.getClass());
			
		}
		
	}
	
	public Map<String, Command> getCommands() {
		return commands;
	}

}
