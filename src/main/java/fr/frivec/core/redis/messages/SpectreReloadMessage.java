package fr.frivec.core.redis.messages;

import java.util.HashMap;

import fr.frivec.core.spectre.ServerType;

public class SpectreReloadMessage {
	
	private HashMap<ServerType, String[]> motd;
	private HashMap<ServerType, Integer> maxSize;
	
	public SpectreReloadMessage() {
		
		this.motd = new HashMap<>();
		this.maxSize = new HashMap<>();
		
	}
	
	public HashMap<ServerType, String[]> getMotd() {
		return motd;
	}
	
	public void setMotd(HashMap<ServerType, String[]> motd) {
		this.motd = motd;
	}
	
	public HashMap<ServerType, Integer> getMaxSize() {
		return maxSize;
	}
	
	public void setMaxSize(HashMap<ServerType, Integer> maxSize) {
		this.maxSize = maxSize;
	}

}
