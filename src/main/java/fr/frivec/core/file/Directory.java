package fr.frivec.core.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Comparator;

public class Directory {
	
	public static void copyDirectory(String sourceDirectoryLocation, String destinationDirectoryLocation) throws IOException {
		
		Files.walk(Paths.get(sourceDirectoryLocation)).forEach(source -> {
			
			Path destination = Paths.get(destinationDirectoryLocation, source.toString().substring(sourceDirectoryLocation.length()));
			
			try {
			
				Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
			
			} catch (IOException e) {
			
				e.printStackTrace();
			
			}
	
		});
	}
	
	public static void deleteFolder(final Path path) throws IOException {
		
		Files.walk(path).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
		
	}

}
