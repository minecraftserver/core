package fr.frivec.core.database;

import com.mongodb.MongoCredential;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Database {
	
	private final MongoClient client; //Client
	private final MongoDatabase database; //Database in the client
	
	public Database(final String host, final String dbName) {

		this.client = new MongoClient(new ServerAddress(host, 27017), MongoCredential.createCredential("API", dbName, "PAh,633/ndF]".toCharArray()), MongoClientOptions.builder().build()); //Instantiate the client
		this.database = this.client.getDatabase(dbName);
		
	}
	
	public Database(final String host, final String dbName, final CodecRegistry customRegistry) {

		//Instantiate the client
		this.client = new MongoClient(new ServerAddress(host, 27017), MongoCredential.createCredential("API", dbName, "PAh,633/ndF]".toCharArray()), MongoClientOptions.builder().codecRegistry(CodecRegistries.fromRegistries(com.mongodb.MongoClient.getDefaultCodecRegistry(), customRegistry)).build());
		
		this.database = this.client.getDatabase(dbName);
		
	}
	
	/**
	 * Return a collection or create a collection if it doesn't exist
	 * 
	 * @param collectionName: Name of the collection in the database
	 * @return MongoCollection - Collection with the name entered
	 */
	public MongoCollection<Document> getCollection(final String collectionName) {
		
		return this.database.getCollection(collectionName);
		
	}
	
	public MongoClient getClient() {
		return client;
	}
	
	public MongoDatabase getDatabase() {
		return database;
	}

}
