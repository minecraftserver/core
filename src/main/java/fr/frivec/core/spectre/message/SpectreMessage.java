package fr.frivec.core.spectre.message;

import fr.frivec.core.spectre.ServerType;

import java.util.UUID;

public class SpectreMessage {
	
	private SpectreMessageType type;
	private ErrorType errorType;
	private UUID id, currentProxyID; //the currentProxyID is only used by servers.
	private ServerType serverType;
	private String serverName;
	private int port, maxSize, playerSize;
	private boolean permanent;
	
	public SpectreMessage(final SpectreMessageType type, final ErrorType errorType, final UUID id, final UUID currentProxyID, final ServerType serverType, final String serverName, final int port, final int maxSize, final int playerSize, final boolean permanent) {
		
		this.type = type;
		this.errorType = errorType;
		this.id = id;
		this.currentProxyID = currentProxyID;
		this.serverType = serverType;
		this.serverName = serverName;
		this.port = port;
		this.maxSize = maxSize;
		this.playerSize = playerSize;
		this.permanent = permanent;
		
	}
	
	public SpectreMessageType getType() {
		return type;
	}

	public void setType(SpectreMessageType type) {
		this.type = type;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getCurrentProxyID() {
		return currentProxyID;
	}

	public void setCurrentProxyID(UUID currentProxyID) {
		this.currentProxyID = currentProxyID;
	}

	public ServerType getServerType() {
		return serverType;
	}

	public void setServerType(ServerType serverType) {
		this.serverType = serverType;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public int getPlayerSize() {
		return playerSize;
	}

	public void setPlayerSize(int playerSize) {
		this.playerSize = playerSize;
	}
	
	public ErrorType getErrorType() {
		return errorType;
	}
	
	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}
	
	public boolean isPermanent() {
		return permanent;
	}
	
	public void setPermanent(boolean permanent) {
		this.permanent = permanent;
	}

	public enum SpectreMessageType {
		
		START_SERVER,
		STOP_SERVER,
		REGISTER_SERVER,
		UNREGISTER_SERVER,
		CREATE_SERVER,
		ERROR;
		
	}
	
	public enum ErrorType {
		
		PROXY_NOT_FOUND,
		SERVER_NOT_FOUND,
		MODEL_NOT_FOUND,
		CREATION_ERROR;
		
	}

}
