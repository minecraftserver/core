package fr.frivec.core.spectre;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.UUID;

import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;

import fr.frivec.core.json.GsonManager;

public class ServerInformations {
	
	public static String redisKey = "Spectre:LocalProcess";
	
	private String name;
	private UUID id, proxyID;
	private String address;
	private int port, size, maxSize;
	private ServerType serverType;
	private String serverStatus;
	private String[] motd;
	private HashSet<UUID> serversInProxy,
							onlinePlayers;

	public ServerInformations(final String name, final UUID id, final UUID proxyID, final String address, final int port, final int maxSize, final ServerType serverType, final String[] motd, final HashSet<UUID> serversInProxy) {
		
		this.name = name;
		this.id = id;
		this.proxyID = proxyID;
		this.address = address;
		this.port = port;
		this.size = 0;
		this.maxSize = maxSize;
		this.serverType = serverType;
		this.motd = motd;
		this.serversInProxy = serversInProxy;
		this.onlinePlayers = new HashSet<>();
	
	}
	
	public static ServerInformations getInfos(final RedissonClient client, final String name) {
		
		final RMap<String, String> map = client.getMap(redisKey);
		
		if(name != null) {
		
			for(Entry<String, String> entries : map.entrySet()) {
				
				final ServerInformations infos = (ServerInformations) GsonManager.getInstance().deSeralizeJson(entries.getValue(), ServerInformations.class);
				
				if(infos.getName().equalsIgnoreCase(name))
					
					return infos;
				
			}
			
		}
		
		return null;
		
	}
	
	public static ServerInformations getInfos(final RedissonClient client, final UUID uuid) {
		
		final RMap<String, String> map = client.getMap(redisKey);

		if(map.containsKey(uuid.toString()))
			
			return (ServerInformations) GsonManager.getInstance().deSeralizeJson(map.get(uuid.toString()), ServerInformations.class);
		
		else
			
			return null;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
	public UUID getProxyID() {
		return proxyID;
	}
	
	public void setProxyID(UUID proxyID) {
		this.proxyID = proxyID;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public int getSize() {
		return size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public ServerType getServerType() {
		return serverType;
	}

	public void setServerType(ServerType serverType) {
		this.serverType = serverType;
	}
	
	public String[] getMotd() {
		return motd;
	}
	
	public void setMotd(String[] motd) {
		this.motd = motd;
	}
	
	public HashSet<UUID> getServersInProxy() {
		return serversInProxy;
	}
	
	public void setServersInProxy(HashSet<UUID> serversInProxy) {
		this.serversInProxy = serversInProxy;
	}
	
	public String getServerStatus() {
		return serverStatus;
	}
	
	public void setServerStatus(String serverStatus) {
		this.serverStatus = serverStatus;
	}

	public HashSet<UUID> getOnlinePlayers() {
		return onlinePlayers;
	}

	public void setOnlinePlayers(HashSet<UUID> onlinePlayers) {
		this.onlinePlayers = onlinePlayers;
	}
}
