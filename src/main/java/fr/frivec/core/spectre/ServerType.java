package fr.frivec.core.spectre;

public enum ServerType {
	
	HUB("HUB"),
	SKYFALL("SKYFALL"),
	PROXY("PROXY");
	
	private String modelName;
	
	private ServerType(final String modelName) {
		
		this.modelName = modelName;
		
	}
	
	public static ServerType getTypeByModel(final String modelName) {
		
		for(ServerType serverType : values())
			
			if(serverType.getModelName().equalsIgnoreCase(modelName))
				
				return serverType;
		
		return null;
		
	}
	
	public String getModelName() {
		return modelName;
	}

}
