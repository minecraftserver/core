package fr.frivec.core.process;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;

import fr.frivec.core.json.GsonManager;
import fr.frivec.core.redis.Redis;
import fr.frivec.core.spectre.ServerInformations;
import fr.frivec.core.spectre.ServerType;

public class LocalProcessUtils {
	
	public static ServerInformations getProcessInformation(final UUID uuid) {
		
		final RedissonClient client = Redis.getInstance().getClient();
		final RMap<String, String> rMap = client.getMap(ServerInformations.redisKey);
		
		if(rMap.containsKey(uuid.toString()))
			
			return (ServerInformations) GsonManager.getInstance().deSeralizeJson(rMap.get(uuid.toString()), ServerInformations.class);
		
		else
			
			return null;
		
	}
	
	public static List<ServerInformations> getListProcessInformations(final ServerType serverType){
		
		final RedissonClient client = Redis.getInstance().getClient();
		final RMap<String, String> rMap = client.getMap(ServerInformations.redisKey);
		final List<ServerInformations> processList = new ArrayList<>();
			
		for(String json : rMap.values()) {
			
			final ServerInformations info = (ServerInformations) GsonManager.getInstance().deSeralizeJson(json, ServerInformations.class);
				
			if(serverType == null || (serverType != null && serverType.equals(info.getServerType())))
				
				processList.add(info);
			
		}
		
		return processList;
		
	}

}
